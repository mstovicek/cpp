#include <iostream>

using namespace std;

// protoze trida ma virtualni metodu, je tzv. polymorfni
class Chytra {
    public:
        // metzoda, kterou maji obe tridy spolecnou - dedi ji
        void run() {
            cout << "spoustim" << endl;

            // vola algoritmus - vypis
            // pokud neni virtual, prekladac pri kompilaci ulozi adresu Chytra.algoritmus() a nezavola pak Hloupa.algoritmus
            algoritmus();
        }
        // pokud je virtual, neuklada se adresa pri kompilaci, ale zjistuje se za behu
        virtual void algoritmus() {
            cout << "chytra" << endl;
        }
};

class Hloupa : public Chytra {
    public:
        // toto je tzv. predefinovana virtualni metoda
        // protoze je metoda virtualni v predkovi, je automaticky virtualni i v potomkovi
        void algoritmus() {
            cout << "hloupa" << endl;
        }
};

int main()
{
    Chytra ch;
    ch.run();

    Hloupa h;
    h.run();

    return 0;
}
