#include <iostream>

using namespace std;

class Trida {
    // default private
    int n, d;

    public:
        Trida(int i, int j) {
            n = i;
            d = j;
        }
        // spratelena funkce - funkce jeDelitel ma pristup ke vsem atributum (i privatnim) ve tride Trida
        friend int jeDelitel(Trida t);
};

int jeDelitel(Trida t) {
    return !(t.n % t.d);
}

int main() {
    Trida t1(10, 2);
    Trida t2(13, 3);

    if(jeDelitel(t1)) {
        cout << "10 je delitelne 2\n";
    }
    else {
        cout << "10 neni delitelne 2\n";
    }


    if(jeDelitel(t2)) {
        cout << "13 je delitelne 3\n";
    }
    else {
        cout << "13 neni delitelne 3\n";
    }

    return 0;
}
