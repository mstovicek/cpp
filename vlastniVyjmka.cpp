#include <iostream>

using namespace std;

// vyjmka je trida, dedi z exception
class DeleniNulou:public exception{
public:
	// konstruktur s implicitnim textem, ted nevim jak se to rekne, ale je spojeny s metodou msg() rodice
	DeleniNulou(const string m = "deleni nulou"):msg(m) {}
	// destruktor, u nej musi byt throw()
	~DeleniNulou(void) throw() {}
	// metoda what vypise vlastni chybu predanou kontruktoru
	const char* what() {
		return msg.c_str();
	}
private:
	// vlastni text ulozeny v privatnim atributu
	string msg;
};

int main()
{
	double operandA = 2.0;
	double operandB = 0;
	double vysledek;
	try{
		// mam radsi zapis stylu "kontrola vstupnich parametru"
		if(operandB == 0) {
			// muze nebo nemusi byt v parametru konstruktoru vlastni hlaska
			//throw DeleniNulou();
			throw DeleniNulou("nula!!");
		}

		// a po kontrole uz jedu normalne bez podminek
		vysledek = 11 / 3;
		cout << vysledek << endl;
	} catch(DeleniNulou& t) {
		cout << t.what() << endl;
	}

	return 0;
}
