#include <iostream>
#include <iomanip>

using namespace std;

class Predek {
    public:
        Predek() {
            cout << "+ Predek" << endl;
        }
        // destructor
        virtual ~Predek() {
            cout << "- Predek" << endl;
        }
};

class Potomek : public Predek {
    public:
        Potomek() {
            cout << "+ Potomek" << endl;
        }
        ~Potomek() {
            cout << "- Potomek" << endl;
        }
 };

int main() {
    // ukazatel typu Predek - ale vytvarim potomka
    Predek *p_typPredek = new Potomek();

    // pokud neni destruktor virtualni, nepozna, ze v pointeru typu Predek mam Potomka
    // a nevola proto desktruktor potomka
    delete p_typPredek;

    return 0;
}