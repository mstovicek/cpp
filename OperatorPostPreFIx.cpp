#include <iostream>
using namespace std;

enum Den{PONDELI = 1, UTERY, STREDA, CTVRTEK, PATEK, SOBOTA, NEDELE};

Den operator++(Den& den){
	if(den == NEDELE){
		den = PONDELI;
		return den;
	}
	den = (Den)(den+1);
	return den;	
}

Den operator++(Den& den, int){
	Den pom = den;
	if(den == NEDELE){
		den = PONDELI;	
	}else{
		den = (Den)(den+1);
	}
	return pom;
}

int main(){
	Den d = PATEK;
	Den cd;
	cd = ++d;
	cout << cd << " " << d << endl;
	d = PATEK;
	cd = d++;
	cout << cd << " " << d << endl;
	system("pause");
	return 0;
}