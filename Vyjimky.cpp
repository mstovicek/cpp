/*
Vyjimka (anglicky exception) predstavuje situaci, ktera nastane v prubehu normalniho behu programu a zpusobi,
ze program nemuze dal pokracovat normalnim zpusobem. Vyjimka je tedy chyba v b�hu programu. Pokud vznikne vyjimka
je treba ji programove osetrit. Vyjimky osetrujeme zpravidla na miste jejich vzniku. Je nutne se snadno, rychle
a bezpecne presunout na jine misto v programu. Snadny presun do jine funkce (primo ci neprimo nadrazene) zajisti
tzv. dlouhy skok. Ten lze provest pomoci standardnich funkci setjmp a longjmp. Ty zajisti zruseni vsech lokalnich promennych
ve vsech blocich, ktere opusti, nezajisti uvolneni prostredku napr. otevreny soubor. Mechanismus prace s vyjimkami v cpp
zajistuje volani destruktoru vsech lokalnich instanci. Lze rovnez prenest informace z mista chyby do mista jejiho zpracovani, 
ktere lze vyuzit pro zpracovani. Vyjimky jazyk C nezna, ale pouziva tzv. strukturovane vyjimky od Mrkvosoftu. 

Jazyk c++ pracuje s tzv. synchronnimi vyjimkami - vyjimky, ktere vzniknou uvnitr programu, nejsou zavisle na OS. 
Nelze tedy zpracovat vyjimky napr. deleni nulou -> asynchronni, osetreni pomoci strukturovanych vyjimek. 

Vsechny operace, ktere by mohly vyvolat vyjimku se musi provadet v tzv. zkusebnim bloku (try block). Blok obsahuje
jeden nebo vice handleru. Pokud nenastane v bloku vyjimka, jsou tyto handlery preskoceny a program pokracuje normalne.
Pokud nastane vyjimky, vykonavani dalsich operaci se prerusi a program se presune do nektereho handleru. Pokud handler neukonci
beh programu, pokracuje program za pokusnym blokem. Try bloky mohou byt vnorene. Vznikne-li vyjimka ve vnitrnim bloku, 
osetrena je pak ve vnejsim bloku -> sireni vyjimky. Pri volani vyjimky se casto vytvari datovy objekt, ktery nese 
informace o povaze a okolnostech vzniku vyjimky. Typ hodnoty, ktera je posilana handleru se nazyva typ vyjimky. 

Syntaxe:
try {blok prikazu}seznam handleru;

Priklad:
void TridaA::vypisPoleNaIndexu(int index){
	try{
		if(index > 5){
			throw 1;
		}else if(index < 0){
			throw 2;
		}else{
			cout << pole[index] << endl;
		}			
	}catch(int i){
		switch(i){
			case 1:
				cout << "Index je vetsi nez pet!" << endl;
				break;
			case 2:
				cout << "Index je mensi nez nula!" << endl;
				break;
		}
	}	
}

Handler musi nasledovat bezprostredne za slozenym prikazem pokusneho bloku nebo za jinym handlerem. 
Handler zacina klicovym slovem catch, za kterym je specifikovan typ vyjimky. Catch(...) univerzalni vyjimka tvz. vypustka.

*/