#include <iostream>
using namespace std;

// pretizeny operator new
void* operator new[](size_t size){
		void *pole = malloc(size);
		// pokud by nebylo memset, bude pamet plna nahodnych cisel
		memset(pole, 0, size);
		return pole;
}

int main()
{
	int *a = (int*) new int[5];
		for(int i = 0; i < 5; i++){
				cout << a[i] << endl;
		}
		system("pause");
		return 0;
}

