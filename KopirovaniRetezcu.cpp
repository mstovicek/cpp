#include <iostream>
// hlavicka string pro pouziti strcpy
#include <string.h>

using namespace std;

int main()
{
	char str1[10] = "";
	char str2[] = "test";

	/**
	 * strcpy (char * dest, cost char * source)
	 */
	strcpy(str1, str2);

	cout << str1 << endl;

	return 0;
}
