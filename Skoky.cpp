#include <iostream>

using namespace std;


int main()
{
    int a = 1;
    cout << a << endl;

    goto Next;

    // nelze pouzit, protoze v neni v bloku:
    // int b = 2;

    // je nutne zvlast deklatovat a zvlast inicializovat
    int b;
    b = 2;
    cout << b << endl;

    Next:

    int c = 3;
    cout << c << endl;

    cout << "---" << endl;

    int x = 10;
    cout << x << endl;

    goto Next2;

    // pokud je y v uzavrenem bloku, lze pouzis deklaraci s inicializaci
    blok: {
        int y = 11;
        cout << y << endl;
    }

    Next2:

    int z = 12;
    cout << z << endl;

    return 0;
}
