#include <iostream>
#include <string.h>

using namespace std;

int main(){
	int a[20];
	// memset nastavi hodnotu kazdeho bajtu v pameti na hodnotu druheho parametru
	memset(a, 7, sizeof(a));
	// v cyklu projde pole a vypise hodnoty
	for (int i = 0; i < sizeof(a) / sizeof(int); i++) {
		cout << *(a + i) << endl;
	}


}

