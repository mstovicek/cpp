/**
 * Lukas
 */

#include <iostream>

using namespace std;


template<class T, template<class V> class W> class Sablona{
	W<T> a;
	public:
		Sablona(W<T> _a) : a(_a) {}
		W<T> vrat();
};

template<class T, template<class V> class W> W<T> Sablona<T,W>::vrat(){
	return a;
}

template<class T> struct TB{
	T x;
};

int main(){
	TB<int> a = {5};
	Sablona<int, TB> b(a);
	cout << b.vrat().x<< endl;
}

