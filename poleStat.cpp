#include <iostream>

using namespace std;


int main()
{
    // 1D pole

    int pole[] = {1, 2, 3, 4, 5};

    // pole je ukazatel na prvni prvek v poli
    int *p_pole = pole;

    cout << pole[1] << endl;
    cout << *(pole + 1) << endl;
    cout << *(p_pole + 1) << endl;

    // 2D pole

    int pole2D[][2] = {{1, 2}, {3, 4}, {5, 6}};

    cout << pole2D[1][1] << endl;
    cout << *(*(pole2D+1)+1) << endl;

    return 0;
}
