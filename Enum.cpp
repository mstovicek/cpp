#include <cstdlib>
#include <iostream>

using namespace std;

typedef enum Barva{
    CERNA = 1, HNEDA, RUZOVA
} b;

int main()
{
    // v cpp je potreba cislo pretypovat na (Barva) a neni potreba rikat, ze jde o enum
    Barva b = (Barva) 3;

    if(b == RUZOVA) {
        cout << "ano" <<endl;
    }

    cout << sizeof(Barva);

    cout << "end";

    return 0;
}
