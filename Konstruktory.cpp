#include <iostream>
using namespace std;
	
class Trida{
public:
	int cisilko;
	//implicitni konstruktor
	Trida(int i){
		this->cisilko = i;
	}

	//kopirovaci konstruktor
	Trida(const Trida& t){
		cout << this->cisilko << endl; //L-value,nedefinovan atribut
		cout << t.cisilko << endl; //R-value = 5 
		cisilko = t.cisilko;
		cout << this->cisilko << endl; //L-value nyni 5
	}
};

int main(){
	Trida a(5);
	Trida b = a;
	
	system("pause");
	return 0;
}