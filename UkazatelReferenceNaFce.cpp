#include <iostream>

using namespace std;

int soucet(int a, int b) {
    return (a+b);
}

int main()
{

    int vysledek;

    // ukazatel na funkci soucet
    int (*p_soucet)(int a, int b) = soucet;

    vysledek = soucet(2, 3);
    cout << vysledek << endl;

    vysledek = p_soucet(2, 3);
    cout << vysledek << endl;

    // reference na funkci soucet
    int (&r_soucet)(int a, int b) = soucet;

    vysledek = soucet(3, 4);
    cout << vysledek << endl;

    vysledek = r_soucet(3, 4);
    cout << vysledek << endl;

    return 0;
}
