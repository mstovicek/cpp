#include <iostream>
#include "demoKonstruktoryOperatory.h"

using namespace std;

/**
 * Implicitni konstruktor
 */
Trida::Trida() {
}

/**
 * Parametricky konstruktor
 */
Trida::Trida(int cislo) {
	this->cislo = cislo;
}

/**
 * Kopirovaci konstruktor - slouzi napr. pro vytvoreni instance
 na zaklade jine instance do L-value (this) je prirazena kopie R-value
 */
Trida::Trida(const Trida& kopie) {
	this->cislo = kopie.cislo;
}

/**
 * Destruktor
 */
Trida::~Trida() {
}

/**
 * Verejna metoda nastavujici cislo
 */
void Trida::setCislo(int cislo) {
	this->cislo = cislo;
}

/**
 * Verejna metoda vracejici cislo
 */
int Trida::getCislo() {
	return this->cislo;
}

/**
 * Kopirovaci operator prirazeni - A = B - instanci je prirazena jina instance
 *
 * Kopírovací operátor přiřazení má smysl definovat, pokud se jedná o třídu,
 * pro níž nemohl překladač vytvořit implicitní kopírovací operátor přiřazení
 * nebo pokud obsahuje dynamicky alokovaný atribut.
 */
Trida& Trida::operator=(const Trida &kopie) {
	this->cislo = kopie.cislo;
	return *this;
}

/**
 * Operator rovnosti
 */
bool Trida::operator==(const Trida &druha) const {
	return this->cislo == druha.cislo;
}

/**
 * Operator nerovnosti
 */
bool Trida::operator!=(const Trida &druha) const {
	return this->cislo != druha.cislo;
}

/**
 * Operator scitani
 */
Trida Trida::operator+(const Trida &druha) const {
	Trida novaTrida;
	novaTrida.setCislo(this->cislo + druha.cislo);
	return novaTrida;
}

/**
 * Operator odecitani
 */
Trida Trida::operator-(const Trida &druha) const {
	Trida novaTrida;
	novaTrida.setCislo(this->cislo - druha.cislo);
	return novaTrida;
}

/**
 * Operator nasobeni
 */
Trida Trida::operator*(const Trida &druha) const {
	Trida novaTrida;
	novaTrida.setCislo(this->cislo * druha.cislo);
	return novaTrida;
}

/**
 * operator inkrementace - prefixovy
 */
Trida& Trida::operator++() {
	this->cislo++;
	return *this;
}

/**
 * operator inkrementace - postfixovy
 */
Trida Trida::operator++(int) {
	Trida temp = *this;
	++*this;
	return temp;
}

/**
 * operator dekrementace - prefixovy
 */
Trida& Trida::operator--() {
	this->cislo--;
	return *this;
}

/**
 * operator dekrementace - postfixovy
 */
Trida Trida::operator--(int) {
	Trida temp = *this;
	--*this;
	return temp;
}

/**
 * operator volani funkce
 */
double Trida::operator() (double a, double b) {
	return a + b;
}

/**
 * operator indexace
 */
int& Trida::operator[] (int index) {
	if (index < 0 || index > sizeof(this->pole) / sizeof(int) - 1) {
		return this->pole[sizeof(this->pole) / sizeof(int) - 1];
	}
	return this->pole[index];
}

/**
 * Hlavni funkce
 */
int main() {
	// pouziti parametrickeho konstruktoru
	Trida t1(5);
	cout << "cislo1 = " << t1.getCislo() << ";" << endl;

	// pouziti copy konstruktoru
	Trida t2(t1);
	cout << "cislo2 = " << t2.getCislo() << ";" << endl;

	// pouziti operatoru prirazeni
	Trida t3;
	t3 = t1;
	cout << "cislo3 = " << t3.getCislo() << ";" << endl;

	// pouziti operatoru inkrementace
	int cisloPost = (t3++).getCislo();
	cout << "cislo3 => cislo: " << t3.getCislo() << " => " << cisloPost << ";" << endl;

	// pouziti operatoru inkrementace
	int cisloPre = (++t3).getCislo();
	cout << "cislo3 => cislo: " << t3.getCislo() << " => " << cisloPre << ";" << endl;

	// pouziti operatoru dekrementace
	cisloPost = (t3--).getCislo();
	cout << "cislo3 => cislo: " << t3.getCislo() << " => " << cisloPost << ";" << endl;

	// pouziti operatoru dekrementace
	cisloPre = (--t3).getCislo();
	cout << "cislo3 => cislo: " << t3.getCislo() << " => " << cisloPre << ";" << endl;

	// porovnani cisel
	cout << "cislo1 (" << t1.getCislo() << ") " << (t1 == t3 ? "==" : "!=") << " cislo3 (" << t3.getCislo() << ")" << endl;

	// porovnani cisel
	cout << "cislo1 (" << t1.getCislo() << ") " << (t1 != t3 ? "!=" : "==") << " cislo3 (" << t3.getCislo() << ")" << endl;

	// operator scitani
	Trida soucet;
	soucet = (t1 + t3);
	cout << "cislo1 (" << t1.getCislo() << ") + cislo3 (" << t3.getCislo() << ") = " << soucet.getCislo() << endl;


	// operator odecitani
	Trida rozdil;
	rozdil = (t1 - t3);
	cout << "cislo1 (" << t1.getCislo() << ") - cislo3 (" << t3.getCislo() << ") = " << rozdil.getCislo() << endl;

	// operator soucinu
	Trida soucin;
	soucin = (t1 * t3);
	cout << "cislo1 (" << t1.getCislo() << ") * cislo3 (" << t3.getCislo() << ") = " << soucin.getCislo() << endl;

	// operator volani funkce
	cout << "2 + 3 = " << t1(2, 3) << endl;

	// operator indexace
	cout << t1[-2] << " - " << t1[2] << " - " << t1[10];

	return 0;
}