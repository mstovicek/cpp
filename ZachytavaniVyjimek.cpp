#include <iostream>
#include <stdio.h>
#include <string>
using namespace std;

class TVyjimka1 {
public:
	virtual void Vypis() const {
		cout << "Vyjimka typu 1\n";
	}
};

class TVyjimka2 : public TVyjimka1 {
public:
	virtual void Vypis() const {
		cout << "Vyjimka typu 2\n";
	}
};

class TVyjimka3 : public TVyjimka2 {
public:
	virtual void Vypis() const {
		cout << "Vyjimka typu 3\n";
	}
};

class TVyjimka4 : public TVyjimka1 {
public:
	virtual void Vypis() const {
		cout << "Vyjimka typu 4\n";
	}
};

int main(){
	cout << "---" << endl;

	try {
		throw TVyjimka3();
	// vyjmky se zachytavaji v poradi: nejdrive potomek
	} catch (TVyjimka3 t) {
		t.Vypis();
	// potom rodic
	} catch (TVyjimka1 t) {
		t.Vypis();
	}

	cout << "---" << endl;

	try {
		throw TVyjimka3();
	// pokud pouziju &, staci chytat rodice, nevytvari se novy objekt vyjmky, ale pouzije se ta vyhozena (trojka)
	} catch (TVyjimka1& t) {
		t.Vypis();
	}

	cout << "---" << endl;

	return 0;
}

