/*
Jazyk c++ umoznuje moznost rozsirit definici vetsiny operatoru take na objektove a vyctove typy.
Nelze tedy vytvaret vlastni operatory a nebo menit operatory pro vestavene typy (krome new a delete).
Nelze menit asociativitu a prioritu operatoru.
Nelze menit pocet operandu (krome new a delete).
Byva zvykem nemenit puvodni vyznam operatoru. Scitani by melo scitat, i kdyz jde udelat, aby nasobilo.
Rozdelujeme je do ctyr skupin:

1.	ternalni operator ?:
	operator primeho pristupu .
	operator dereferencovani tridnich ukazatelu *.
	rozlisovaci operator ::
	type_id, static_cast, dynamic_cast, reinterpret_cast, const_cast
- jestli jsem to spravne pochopil, ani jeden z nich nelze pretypovat

2.	operator neprime kvalifikace ->
	operator pretypovani (typ)
	operator volani funkce ()
	operator prirazeni =
	operator indexace []
- lze je pretizit jako nestaticke clenske funkce (metody)

3. operatory new a delete
- lze je pretizit jako obycejne funkce nebo jako staticke metody obj. datovych typu

4. ostatni operatory
- lze je pretizit jako obycejne funkce s min. jednim parametrem objektoveho nebo vyctoveho 
  typu nebo nestaticke clenske funkce


  Unarni operator - pretizit jako nestaticka clenska funkce bez parametru (operand je instance tridy) 
					nebo jako obycejna funkce s jednom parametrem (napr. ++/--)
  Binarni operator -pretizit jako nestaticka clenska funkce s jednim parametrem 
					nebo jako obycejna funkce se dvema parametry - alespon jeden objektoveho nebo vyctoveho typu 
					(napr. +,-,*)











*/