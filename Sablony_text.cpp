/*
Sablony (anglicky templates) jsou zakladem generickeho programovani. Genericke programovani je jeden ze stylu programovani,
je zalozeno na genericke konstrukci (sablony). Dalím typem programovaciho stylu je strukturovane programovani, jedna se o programovani 
funkci, ktere pracuji nad daty - neobjektovy pristup. Poslednim stylem je objektove orientovane programovani, kde se pracuje s objekty a jejich vlastnostmi.

Sablony jsou vzory podle nichz prekladac vytvori funkci nebo objektovy typ se skutecnymi parametry. Sablony funguji podobne jako makra,
ale poskytuji vice moznosti. Narozdil od maker je zpracovava prekladac. 
Sablony popisuji celou mnozinu funkci, ktere se lisi napr. jen typem parametru, nebo popisuji celou mnozinu objektoveho typu, 
kde se lisi atributy datovym typem. 

Chceme-li vytvorit sablonu s konkretni datovym typem, pak mluvime o tzv. instanci sablony. 
Existuje STL - standartni knihovna sablon - proc taky programovat neco co uz nekdo programoval
Syntaxe sablony:

template <seznam_parametru>

- seznam parametru, kazdy par. je oddelen carkou, byva zvykem pouzivat class T nebo typename T - muze se ale jmenovat skoro libovolne
- kazdy parametr lze chapat jako promenou
- parametrem mohou byt hodnotove typy, formalni typy nebo sablony jinycho objektovych typu

Deklarace sablony
Deklarace sablony se v programu muue objevit na urovni souboru, uvnitr objektoveho typu nebo
uvnitr sablony objektového typu. Sablona se nemuze deklarovat jako lokalní v bloku.

priklad:
-mejme tridu stack (samozrejme existuje v STL ale o to nejde)

class Stack{								
	int *pole;
	stack(){
		pole = new int[5];
	}
	void nastav(int co, int kam){
		pole[kam] = co;
	}
	int vrat(int kde){
		return pole[kde];
	}
};

Tuto jednoduchou tridu predelame na sablonu:

template<class T>
class Stack{
	T *pole;
	stack(){
		pole = new T[5];
	}
	void nastav(T co, int kde){
		pole[kde] = co;
	}
	T vrat(int kde){
		return pole[kde];
	}
};

puvodni instance:
	Stack s;
instance sablony:
	Stack<int> sablonaInt;
	Stack<string> sablonaStringu;
	
definice metody mimo sablonu:
template<class T>
class Stack{
	T *pole;
	stack(){
		pole = new T[5];
	}
	void nastav(T co, int kde){
		pole[kde] = co;
	}
	T vrat(int kde);	
};

template<class T>
T zasobnik<T>::vrat(int kde){
	return pole[kde];
}

!!! jmena mohou byt ruzna, ale poradi parametru musi sedet

Sablony funkce - funkce, ktera vraci maximum
template<class T>
T maximum(T a, T b){
	if(a > b){
		return a;
	}
	return b;
}
cout << max<int>(5,6) << endl;
nebo
cout << max(5,6) << endl;

Explicitni specializace sablony umoznuje deklarovat specifickou verzi sablony pro její urcite
skutecne parametry.

syntaxe:
template<> deklarace

priklad:
template<class T> void vypis(T a, T b){}
template<> void vypis(double a, double b){} - explicitni
*/