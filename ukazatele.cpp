#include <iostream>

using namespace std;

int main() {
    int a = 10;
    int* p_a = &a;

    int b = 20;
    int* p_b = p_a;

    cout << p_a << endl;
    cout << p_b << endl;
    cout << a << endl;
    cout << b << endl;
    cout << *p_a << endl;
    cout << *p_b << endl;
    cout << endl;

    *p_b = 2;

    cout << p_a << endl;
    cout << p_b << endl;
    cout << a << endl;
    cout << b << endl;
    cout << *p_a << endl;
    cout << *p_b << endl;
    cout << endl;

    a = 3;

    cout << p_a << endl;
    cout << p_b << endl;
    cout << a << endl;
    cout << b << endl;
    cout << *p_a << endl;
    cout << *p_b << endl;
    cout << endl;

    // velikost ukazatele zavisi na typu kompilatoru (64bit PC ma velikost pointeru 8B)
    // velikost ukazatele je pro kompilator vzdy stejna:
    // sizeof(int*) = sizeof(void*) = sizeof(double*) = ...

    return 0;
}
