/**
 * Priklad tridy s konstruktory, destruktorem, atributy, metodami, operatory
 */
class Trida {
	// soukrome atributy a metody
	private:

		// soukromy atribut
		int cislo;

		// soukromy atribut pole
		int pole[5] = {1, 2, 3, 4, 5};

	// verejne atributy a metody
	public:

		// implicitni (defaultni) konstruktor
		Trida();

		// parametricky konstruktor
		Trida(int);

		// copy konstructor
		Trida(const Trida&);

		// desktruktor
		~Trida();

		// verejne metoda nastavi cislo
		void setCislo(int);

		// verejne metoda vraci cislo
		int getCislo();

		// operator prirazeni
		Trida& operator=(const Trida&);

		// operator rovnosti
		bool operator==(const Trida&) const;

		// operator nerovnosti
		bool operator!=(const Trida&) const;

		// operator scitani
		Trida operator+(const Trida&) const;

		// operator odecitani
		Trida operator-(const Trida&) const;

		// operator nasobeni
		Trida operator*(const Trida&) const;

		// operator inkrementace - prefixovy
		Trida& operator++();

		// operator inkrementace - postfixovy
		Trida operator++(int);

		// operator dekrementace - prefixovy
		Trida& operator--();

		// operator dekrementace - postfixovy
		Trida operator--(int);

		// operator volani funkce
		double operator() (double, double);

		// operator indexace
		int& operator[] (int);
};