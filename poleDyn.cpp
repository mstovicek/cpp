#include <iostream>

using namespace std;


int main()
{
    // alokace dvourozmerneho pole 5x3
    int **pole = (int**) malloc(sizeof(int*) * 5);
    for (int i = 0; i < 5; i++) {
        pole[i] = (int*) malloc(sizeof(int) * 3);
    }

    // prirazeni - druhy radek prvn� sloupec
    *(*(pole+1)) = 5;
    cout << pole[1][0] << endl;

    //dealokace vsech sloupcu
    for (int i = 0; i < 5; i++) {
        free(pole[i]);
    }

    //dealokace nulteho radku
    free(pole);

    return 0;
}
